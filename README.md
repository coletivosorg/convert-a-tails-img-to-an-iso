# Convert a tails img to an iso



## Getting started

First download the latest tails img from their [download page on tails.boum.org](https://tails.boum.org/install/download/index.en.html)

you will need to mount the img file as a folder[2](https://stackoverflow.com/a/47806288):

```
$ mkdir tailsimg
$ sudo mount -o loop,offset=1048576 tails-amd64-5.7.img tailsimg/
```

then copy all files and change permissions [1](https://unix.stackexchange.com/a/316410)
```
$ mkdir tailsiso
$ cp -pr tailsimg/* tailsiso/
$ chmod -R u+w tailsiso/
$ cd tailsiso/
$ mkisofs -o ../tails-amd64-5.7.iso -b syslinux/isolinux.bin -no-emul-boot -boot-load-size 4 -boot-info-table -V "My OWN ISO! tails-amd64-5.7" -R -J -v -T syslinux/. .
```

## Sources

[1 - How to mount a disk img from the command line? - Unix Stackoverflow](https://unix.stackexchange.com/a/316410)
[2 - Building bootable iso using mksisofs - Stackoverflow](https://stackoverflow.com/a/47806288)
[3 - download tails](https://tails.boum.org/install/download/index.en.html)


## Modify the tails image to don't start gnome by default it crashes in virtualbox

```
$ sudo mount tailsiso/live/filesystem.squashfs tailssquash/ -t squashfs -o loop
$ sudo cp -pr tailssquash/* tailssquashcopy/
$ sudo chroot tailssquashcopy
:/# systemctl set-default multi-user.target
:/# adduser usernameuwant
:/# usermod -a -G sudo usernameuwant
:/# exit
$ rm tailsiso/live/filesystem.squashfs
$ sudo mksquashfs tailssquashcopy/ tailsiso/live/filesystem.squashfs
$ cd tailsiso/
$ mkisofs -o ../tails-amd64-5.7.iso -b syslinux/isolinux.bin -no-emul-boot -boot-load-size 4 -boot-info-table -V "My OWN ISO! tails-amd64-5.7" -R -J -v -T syslinux/. .
```

https://askubuntu.com/questions/151840/how-to-disable-gdm-from-being-automatically-started
https://unix.stackexchange.com/a/336840
